<?php
/**
 * @file
 * Contains \Drupal\social_post_twitter_tweet\Controller\SocialPostTwitterTweetController.
 */

namespace Drupal\social_post_twitter_tweet\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\social_post_twitter_tweet\Controller;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Entity\EntityInterface;
use Twitter\Text\Parser;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CommandInterface;

class SocialPostTwitterTweetController extends ControllerBase {

      function TwitterTweet($tweetaccount = NULL,$tweettweet = NULL) {

               if (tweetaccount <> NULL && $tweettweet <> NULL) {
                  // Gets instance of Social Post Twitter.
                  $twitterPost = \Drupal::service('plugin.network.manager')->createInstance('social_post_twitter');
                  // Gets the Social Post manager.
                  /* @var \Drupal\social_post\SocialPostManager $post_manager */
                  $post_manager = \Drupal::service('social_post.post_manager');
                  $post_manager->setPluginId('social_post_twitter');

                  $account = \Drupal\social_post\Entity\SocialPost::load($tweetaccount);

                  $access_token = json_decode($post_manager->getToken($account->getProviderUserId()), TRUE);

                  // We check if social_post_twitter has been patched!
                  if (method_exists($twitterPost,'uploadMedia')) { // If we have uploadMedia method, we'll tweet the image
                     $twitterPost->doPost($access_token['oauth_token'], $access_token['oauth_token_secret'], $tweettweet);
                  } else {
                     $twitterPost->doPost($access_token['oauth_token'], $access_token['oauth_token_secret'], $tweettweet['status']);
                  }
                  // return $result;
                  return NULL;
               }
      }

      function TwitterGoThroughTweets($tweetpara = NULL,$multilingual = NULL) {

               if (($tweetpara->field_date_and_time->value <> NULL && $tweetpara->field_tweeted_on->value == NULL) OR ($tweetpara->field_date_and_time->value == NULL && $tweetpara->field_tweeted_on->value == NULL) ) {
                  if ( $tweettime < time() ) {
                     // Tweet is due for tweeting!!
                     $tweettweet = $tweetpara->field_tweet_text->value;     // The text we want to tweet
                     $tweetaccount = $tweetpara->field_twitter_account->target_id; // the Twitter account ID

                     $node = \Drupal\node\Entity\Node::load($tweetpara->parent_id->value);

                     if ($multilingual) {
                        if ($node->hasTranslation($tweetpara->langcode->value)) {
                           $transnode = $node->getTranslation($tweetpara->langcode->value);
                        } else {
                           $transnode = $node;
                        }
                     }

                     if ( $transnode->isPublished() ) { // changed from node to transnode
                        $tweettweet .= ' '.\Drupal\social_post_twitter_tweet\Controller\SocialPostTwitterTweetController::tweetGetFullPath($node->id(),$tweetpara->langcode->value);

                        // Use field_image as source images for media posting to Twitter.
                        $images = ($tweetpara->hasField('field_tweet_image') && !empty($tweetpara->get('field_tweet_image')->getValue())) ? $tweetpara->get('field_tweet_image')->referencedEntities() : [];
                        $image_paths = [];
                        foreach ($images as $image) {
                          $image_paths[] = drupal_realpath($image->getFileUri());
                        }

                        $tweet = [];
                        $tweet['status'] = $tweettweet;
                        $tweet['media_paths'] = $image_paths;

                        \Drupal\social_post_twitter_tweet\Controller\SocialPostTwitterTweetController::TwitterTweet($tweetaccount,$tweet); // with image
                        //   \Drupal\social_post_twitter_tweet\Controller\SocialPostTwitterTweetController::TwitterTweet($tweetaccount,$tweettweet); // without image
                        
                        // We update the paragraph! We need the field_tweeted_on field, which is a date field
                        $tweettime = date('Y-m-d\TH:i:s',\Drupal::time()->getCurrentTime());
                        $tweetpara->field_tweeted_on->value = $tweettime;
                        $tweetpara->save();
                     } // End tweeting only for published node!
                  }
               }
      }

      /**
       * Sending a tweet given the paragraph ID, nid, and langcode
       */

      function tweetSendTweetPara($tparaid = NULL,$nid = NULL,$langcode = NULL) {
               if ($tparaid && $nid) {
                  // $tpara = \Drupal\paragraphs\Entity\Paragraph::load($tparaid);
                  $tpara = \Drupal\social_post_twitter_tweet\Controller\SocialPostTwitterTweetController::tweetGetParagraph($tparaid,$langcode);

                  $tweetdatetime = $tpara->field_date_and_time->value; // should be NULL
                  $tweettweet = $tpara->field_tweet_text->value;     // The text we want to tweet
                  $tweetaccount = $tpara->field_twitter_account->target_id; // the Twitter account ID

                  $tweettweet .= ' '.\Drupal\social_post_twitter_tweet\Controller\SocialPostTwitterTweetController::tweetGetFullPath($nid, $langcode);

                  // Use field_image as source images for media posting to Twitter.
                  $images = ($tpara->hasField('field_tweet_image') && !empty($tpara->get('field_tweet_image')->getValue())) ? $tpara->get('field_tweet_image')->referencedEntities() : [];

                  $image_paths = [];
                  foreach ($images as $image) {
                     $image_paths[] = drupal_realpath($image->getFileUri());
                  }

                  $tweet = [];
                  $tweet['status'] = $tweettweet;
                  $tweet['media_paths'] = $image_paths;


                  if ( $tweetdatetime == NULL && $tweettweet <> "" && $tpara->field_tweeted_on->value == NULL) {
                     // This should be using the controller, but it is not working yet. My Controller is badly named!!
                     //\Drupal\social_post_twitter_tweet\Controller\SocialPostTwitterTweetController::TwitterTweet($tweetaccount,$tweettweet); // without image
                     \Drupal\social_post_twitter_tweet\Controller\SocialPostTwitterTweetController::TwitterTweet($tweetaccount,$tweet); // with image

                     // We update the paragraph! We need the field_tweeted_on field, which is a date field
                     $tweettime = date('Y-m-d\TH:i:s',\Drupal::time()->getCurrentTime());
                     $tpara->field_tweeted_on->value = $tweettime;
                     $tpara->field_date_and_time->value = $tweettime;
                     $tpara->save();
                     return TRUE;
                   } else {
                     return NULL;
                   }
               }
      }

      /**
       * function to get the right language version of a tweet
       */

      function tweetGetParagraph($tparaid = NULL, $langcode = NULL) {

               $tpara = \Drupal\paragraphs\Entity\Paragraph::load($tparaid);

               if ($tpara->isTranslatable() && $langcode) {
                  if ($tpara->hasTranslation($langcode)) {
                     $tweetpara = $tpara->getTranslation($langcode);
                  } else {
                     $tweetpara = $tpara;
                  }    
               } else {
                  $tweetpara = $tpara;
               }
               // The original language tweet has been tweeted, and the tweeted_on time is set by default
               if ($tpara->field_date_and_time->value == $tpara->field_tweeted_on->value && $tweetpara->field_date_and_time->value == NULL) {
                  $tweetpara->field_tweeted_on->value = NULL;
               }
               return $tweetpara;
      }


      function tweetGetFullPath($nodeid = NULL,$langcode = NULL) {

               if ($nodeid) {
                  $fronturl = \Drupal\Core\Url::fromRoute('<front>');
                  $fronturl->setAbsolute();
                  $frontroot = rtrim($fronturl->toString(),"/");

                  return $frontroot.\Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$nodeid,$langcode);
                } else {
                  return "";
                }

      }

  /**
   * Callback function for Tweet text field
   */

  function validateTweetText(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    $response = new AjaxResponse();
    // We need to figure out which of the paragraphs triggered the ajax call
    $triggering_element = $form_state->getTriggeringElement()['#parents'][1];

    // Getting the value of our tweet text
    $field_value = $form_state->getValue(['field_social_post_tweet',$triggering_element,'subform','field_tweet_text']);
    $field_value = $field_value[0]['value'];
    $field_class = "form-item-field-social-post-tweet-".$triggering_element."-subform-field-tweet-text-0-value";

    // Adding a link at the end.
    $drupalhost = \Drupal::urlGenerator()->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $tweet_text = $field_value.' '.$drupalhost . '/node/1';

    $int_ext = extension_loaded("intl");
    if ($int_ext) {
       // $message .= 'PHP international extension loaded! ';
       $parser = new \Twitter\Text\Parser;
       $result = $parser->parseTweet($tweet_text);

       if ($result->valid) {
          $message = t('Tweet valid.')->render();
          $css = ['background-color' => '#ffffff', 'font-weight' => 'normal'];
       } else {
          $message = t('Tweet not valid!!')->render();
          $css = ['background-color' => '#ff6666', 'font-weight' => 'bold'];
       }

       $tweet_length = $result->weightedLength;
       $remaining    = 280 - $tweet_length;
       $message .= ' '.t('Remaining characters:')->render().' '.$remaining;

    } else {
       $message = t('PHP international extension not loaded! Ask your server administrator to fix this!!')->render();
    }

    // We output some response
    //$response->addCommand(new \Drupal\Core\Ajax\CssCommand($form_state->getTriggeringElement()['#id'], $css));
    $response->addCommand(new \Drupal\Core\Ajax\CssCommand('.tweet-validated-'.$triggering_element, $css));
    $response->addCommand(new \Drupal\Core\Ajax\HtmlCommand('.tweet-validated-'.$triggering_element, $message));

    return $response;
  }

}
