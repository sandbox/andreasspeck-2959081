<?php

namespace Drupal\social_post_twitter_tweet\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a valid Tweet.
 *
 * @Constraint(
 *   id = "valid_tweet",
 *   label = @Translation("Valid Tweet", context = "Validation"),
 *   type = "string"
 * )
 */

class ValidTweet extends Constraint {

  // The message that will be shown if the value is not a valid tweet.
  public $invalidTweet = '%value is not a valid tweet text';

}
