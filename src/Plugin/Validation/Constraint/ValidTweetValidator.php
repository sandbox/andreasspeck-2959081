<?php

namespace Drupal\social_post_twitter_tweet\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Twitter\Text\Parser;

/**
 * Validates the ValidTweet constraint.
 */
class ValidTweetValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {

    $int_ext = extension_loaded("intl");

    foreach ($items as $item) {
      // First check if the value is an integer.
      
      $tweetvalid = false;

      if ($int_ext) {
         // $message .= 'PHP international extension loaded! ';
         $parser = new \Twitter\Text\Parser;
         $result = $parser->parseTweet($item->value.' '.\Drupal::urlGenerator()->generateFromRoute('<front>', [], ['absolute' => TRUE]).'/node/1');

         if ($result->valid) {
            $tweetvalid = true;
         }
      } else {
         $tweetvalid = true; // Make sure we don't break it if int_ext is not installed
      }
      if (!$tweetvalid) {
        // The value is not a valid tweet, so a violation, aka error, is applied.
        // The type of violation applied comes from the constraint description
        // in step 1.
        $this->context->addViolation($constraint->invalidTweet, ['%value' => $item->value]);
      }
    }
  }

}
