The Social Post Twitter Tweet module makes use of the Social Post Twitter module, which in itself does not tweet. This module adds the functionality to schedule several tweets, or to tweet on node creation.

We do not believe it is a good idea to try to create a tweet automatically, based on title and other categories. The results are likely not very satisfying. For this reason, we take a different approach, which gives you full control over what will be tweeted. For each node which has the tweet field added (see Installation below) you can schedule as many tweets as you want, so you can for example schedule a different tweet each day, or for whatever time you want.

The module makes use of accounts configured via the Social Post Twitter module. This module does not allow you to configure a system account, but all Twitter accounts are related to user accounts. For this reasons the Twitter accounts linked to uid1 are considered system accounts.

The module provides a new permission "Can system tweet", which allows a user with that permission to use the uid1 Twitter accounts for tweeting. A user without that permission only can use their own Twitter accounts.

For each Tweet, you can select which of the available Twitter accounts should be used.



INSTALLATION

It is extremely important that you first install Social Post Twitter (follow the instructions at https://www.drupal.org/node/2780771). 

Even though when you just install Social Post Twitter Tweet, it will bring in all the module dependencies, but not the composer dependencies. 
There are none for this module, but Social Post Twitter requires the Twitter OAuth library and the oauth2-client library. If you just enable Social Post Twitter Tweet without BEFORE installing Social Post Twitter the right way, it will not work.

So, if you do not install Social Post Twitter via composer, first only download the module via drush dl social_post_twitter, and before enabling it do:

composer require "abraham/twitteroauth"
composer require "league/oauth2-client:~2.0"

The you enable Social Post Twitter via drush.

Before you can make use of Social Post Twitter Tweet, you need to correctly configure Social Post Twitter. You need to create a Twitter App 
(see "Step 1: Create a Twitter App": https://www.drupal.org/node/2780771).

Social Post Twitter Tweet depends on the twitter-text php library (https://github.com/nojimage/twitter-text-php) for the validation of tweets. Install it via:

composer require "nojimage/twitter-text-php"

Social Post Twitter Tweet creates a new paragraph type "Tweet for social_post_twitter_tweet" and a new field "Tweet", which is an entity_reference_revision field to paragraphs. To be able to schedule tweets for a content type, this field (existing field) needs to be added to your content type. The field needs to reference the paragraph type social_post_twitter_tweet, otherwise you won't have the right data... It is recommended to allow unlimited instances of this paragraph type, so you can schedule several feeds on node creation. This will require for cron to be setup correctly. Scheduled tweets will be tweeted on the first cron run after the scheduled date and time.

Now when you add or edit a node of this content type, you can schedule various tweets.


MULTILINGUAL CONTENT

Paragraphs and multilingual content is a bit buggy. The field_tweet should not be translatable. Instead, you should make the paragraph type translatable - all fields of the paragraph type should be translatable to achieve this.

It seems you can only create a new paragraph when editing the original language version of a node. Only existing paragraphs can be translated. If that can be fixed, that would be nice.




